﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSpawner : MonoBehaviour
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down,
    }

    [SerializeField]
    private GameObject _arrowPrefab;

    [SerializeField]
    private Vector2 _timePerArrowGenerationMinMax;

    [SerializeField]
    private Vector2 _arrowSpeedMinMax;

    [SerializeField]
    private Direction _arrowDirection;

    private float _timeToGenerateArrow;

    // Start is called before the first frame update
    void Start()
    {
        _timeToGenerateArrow = UnityEngine.Random.Range(_timePerArrowGenerationMinMax.x, _timePerArrowGenerationMinMax.y);
    }

    // Update is called once per frame
    void Update()
    {
        _timeToGenerateArrow -= Time.deltaTime;

        if (_timeToGenerateArrow < 0)
        {
            _timeToGenerateArrow = UnityEngine.Random.Range(_timePerArrowGenerationMinMax.x, _timePerArrowGenerationMinMax.y);

            GameObject go = Instantiate<GameObject>(_arrowPrefab);

            if (_arrowDirection == Direction.Left)
            {
                go.GetComponent<ArrowConstantMovementScript>().SetDirection(ArrowConstantMovementScript.Direction.Left);
            }
            else if (_arrowDirection == Direction.Right)
            {
                go.GetComponent<ArrowConstantMovementScript>().SetDirection(ArrowConstantMovementScript.Direction.Right);
            }
            else if (_arrowDirection == Direction.Up)
            {
                go.GetComponent<ArrowConstantMovementScript>().SetDirection(ArrowConstantMovementScript.Direction.Up);
            }
            else if (_arrowDirection == Direction.Down)
            {
                go.GetComponent<ArrowConstantMovementScript>().SetDirection(ArrowConstantMovementScript.Direction.Down);
            }

            go.GetComponent<ArrowConstantMovementScript>().SetSpeed(UnityEngine.Random.Range(_arrowSpeedMinMax.x, _arrowSpeedMinMax.y));

            go.transform.parent = this.transform;
            go.transform.localPosition = Vector3.zero;


        }
    }
}
