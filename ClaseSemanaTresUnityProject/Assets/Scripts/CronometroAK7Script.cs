﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CronometroAK7Script : MonoBehaviour
{
    private float _tiempoHastaElMomento = 0;

    // Start is called before the first frame update
    void Start()
    {
        _tiempoHastaElMomento = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _tiempoHastaElMomento += Time.deltaTime;
        Debug.Log("_tiempoHastaElMomento:" + _tiempoHastaElMomento);
    }
}
