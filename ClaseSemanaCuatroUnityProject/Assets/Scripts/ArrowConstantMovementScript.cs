﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowConstantMovementScript : MonoBehaviour
{
    public enum Direction
    {
        None,
        Left,
        Right,
        Up,
        Down,
    }

    [SerializeField]
    private List<int> _rotations;

    private Direction _currentDirection = Direction.None;

    private float _speed;

    // Start is called before the first frame update
    void Start()
    {
        //_currentDirection = Direction.Left;
    }

    // Update is called once per frame
    void Update()
    {
        if (_currentDirection == Direction.Left)
        {
            this.transform.position += Vector3.left * Time.deltaTime * _speed;
        }
        else if (_currentDirection == Direction.Right)
        {
            this.transform.position += Vector3.right * Time.deltaTime * _speed;
        }
        else if (_currentDirection == Direction.Up)
        {
            this.transform.position += Vector3.up * Time.deltaTime * _speed;
        }
        else if (_currentDirection == Direction.Down)
        {
            this.transform.position += Vector3.down * Time.deltaTime * _speed;
        }
    }

    public void SetDirection(Direction pDirection)
    {
        _currentDirection = pDirection;

        this.transform.eulerAngles = new Vector3(0, 0, _rotations[(int)pDirection]);
    }

    public void SetSpeed(float pSpeed)
    {
        _speed = pSpeed;
    }
}
