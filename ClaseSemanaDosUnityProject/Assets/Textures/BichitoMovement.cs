﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BichitoMovement : MonoBehaviour
{
    public Rigidbody2D _rigidbody;

    private void Awake()
    {
        //Se ejecuta una sola vez y es el primer momento cuando el gameobject que tiene el componente esta activado y en la escena
        //Debug.Log("Awake");
    }

    // Start is called before the first frame update
    void Start()
    {
        //Se ejecuta una sola vez y es el primer momento cuando el componente esta activado y en la escena
        //Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        //Se ejecuta cada frame, entonces si el juego corre a 60 FPS (Frames per Second) se ejecutaria 60 veces por segundo
        //Debug.Log("Update");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    public void Jump()
    {
        _rigidbody.velocity = new Vector3(0f, 5f, 0f);
    }

    public void JumpVeryHigh()
    {
        _rigidbody.velocity = new Vector3(0f, 20f, 0f);
    }
}
