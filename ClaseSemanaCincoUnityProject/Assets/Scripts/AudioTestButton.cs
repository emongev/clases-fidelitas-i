﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioTestButton : MonoBehaviour
{
    [SerializeField]
    private AudioManager.AudioFile _audioFile;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(OnAudioTestButtonPressed);
    }

    private void OnAudioTestButtonPressed()
    {
        AudioManager.Instance.PlayAudio(_audioFile);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
