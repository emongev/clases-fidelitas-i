﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBoxGenScript : MonoBehaviour
{
    private const float TIME_BETWEEN_BOXES = 5f;
    private float _time = 0;

    [SerializeField]
    private GameObject _boxPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;

        if(_time > TIME_BETWEEN_BOXES)
        {
            GenerateBox();
            _time -= TIME_BETWEEN_BOXES;
        }
    }

    private void GenerateBox()
    {
        Instantiate(_boxPrefab, this.transform.position, Quaternion.identity, null);
    }
}
