﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CronometroScript : MonoBehaviour
{
    private int _framesHastaElMomento = 0;
    private int _segundosHastaElMomento = 0;

    private void Start()
    {
        _segundosHastaElMomento = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _framesHastaElMomento++;

        if (_framesHastaElMomento > 60)
        {
            _segundosHastaElMomento++;
            Debug.Log("_segundosHastaElMomento:" + _segundosHastaElMomento);
            _framesHastaElMomento = 0;
        }
    }
}
