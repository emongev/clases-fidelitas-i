﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        NumberBoxes.Instance.IncreaseBoxes();
    }

    // Update is called once per frame
    void OnDestroy()
    {
        NumberBoxes.Instance.ReduceBoxes();
    }
}
