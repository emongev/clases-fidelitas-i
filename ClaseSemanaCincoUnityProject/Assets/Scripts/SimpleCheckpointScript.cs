﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCheckpointScript : MonoBehaviour
{
    [SerializeField]
    private int _checkpointId;

    [SerializeField]
    private Transform _spawnPoint;

    [SerializeField]
    private bool _onlyOnce = false;

    private bool _activated = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetSpawnPointPosition()
    {
        if (_spawnPoint != null)
        {
            return _spawnPoint.position;
        }
        else
        {
            return this.transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Character")
        {
            if (!_onlyOnce || (_onlyOnce && !_activated))
            {
                CheckpointManager.Instance.SetCheckpointAsLast(this);
                _activated = true;
            }
        }
    }
}
