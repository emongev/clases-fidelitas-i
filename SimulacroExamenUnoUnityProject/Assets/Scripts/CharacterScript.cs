﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterScript : MonoBehaviour
{
    [SerializeField]
    private float horSpeed;

    [SerializeField]
    private float jumpSpeed;

    [SerializeField]
    private Transform groundedTestTransform;

    [SerializeField]
    private Transform grabBoxTestTransform;

    List<Transform> _cajas;

    void Awake()
    {
        _cajas = new List<Transform>();
    }

    void Update()
    {
        Vector3 currentVelocity = this.gameObject.GetComponent<Rigidbody2D>().velocity;

        if(Input.GetKey(KeyCode.LeftArrow))
        {
            currentVelocity.x = -horSpeed;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            currentVelocity.x = horSpeed;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(CanJump())
            {
                currentVelocity.y = jumpSpeed;
            }
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            GrabBoxes();
        }
        else
        {
            ReleaseBoxes();
        }

        this.gameObject.GetComponent<Rigidbody2D>().velocity = currentVelocity;
    }

    private bool CanJump()
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(groundedTestTransform.position, new Vector2(0.25f, 0.25f), 0f, Vector2.zero);

        if (hits.Length > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void GrabBoxes()
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(grabBoxTestTransform.position, new Vector2(2f, 2f), 0f, Vector2.zero);

        for (int i = 0; i < hits.Length; i++)
        {
            if(hits[i].collider.gameObject.CompareTag("Caja"))
            {
                hits[i].collider.transform.parent = this.transform;
                hits[i].collider.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                if (!_cajas.Contains(hits[i].collider.transform))
                {
                    _cajas.Add(hits[i].collider.transform);
                }
            }
        }
    }

    private void ReleaseBoxes()
    {
        for (int i = 0; i < _cajas.Count; i++)
        {
            if (_cajas[i] != null)
            {
                _cajas[i].parent = null;
                _cajas[i].gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            }
        }
        _cajas.Clear();
    }
}
