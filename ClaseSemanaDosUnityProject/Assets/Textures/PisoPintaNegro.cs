﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoPintaNegro : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("NOW YOUR COLOR IS BLACK!");
        collision.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        Destroy(this.gameObject);
    }
}
