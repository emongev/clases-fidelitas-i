﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class CenterCharacterScript : MonoBehaviour
{
    public enum Position
    {
        Left,
        Right,
        Up,
        Down,
    }

    [SerializeField]
    private List<Collider2D> _colliderTriggers;

    [SerializeField]
    private TextMeshProUGUI _pointsText;

    private int _points;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            DoBoxCast(_colliderTriggers[(int)Position.Right]);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            DoBoxCast(_colliderTriggers[(int)Position.Left]);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            DoBoxCast(_colliderTriggers[(int)Position.Up]);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            DoBoxCast(_colliderTriggers[(int)Position.Down]);
        }
    }

    private void DoBoxCast(Collider2D pCollider)
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(pCollider.transform.position, pCollider.bounds.extents, 0, Vector3.zero);

        foreach (RaycastHit2D singleHit in hits)
        {
            if (singleHit.collider.tag == "Arrow")
            {
                Debug.Log("name:" + singleHit.collider.gameObject.name);
                _points++;
                Destroy(singleHit.collider.gameObject);
                _pointsText.text = _points.ToString();
            } 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Arrow")
        {
            Destroy(collision.gameObject);
            _points = 0;
            _pointsText.text = _points.ToString();

            this.transform.DOKill();
            this.transform.localPosition = Vector3.zero;
            this.transform.DOShakePosition(0.1f);
            this.GetComponent<SpriteRenderer>().DOKill();
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<SpriteRenderer>().DOColor(Color.red, 0.5f).SetLoops(2, LoopType.Yoyo);
        }
    }


}
