﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    private static CheckpointManager _instance;
    
    public static CheckpointManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private SimpleCheckpointScript _lastCheckpoint;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            CheckpointCharacter character = GameObject.FindObjectOfType<CheckpointCharacter>();

            if (character != null && _lastCheckpoint != null)
            {
                character.transform.position = _lastCheckpoint.GetSpawnPointPosition();
            }
        }
    }

    public void SetCheckpointAsLast(SimpleCheckpointScript pSimpleCheckpointScript)
    {
        _lastCheckpoint = pSimpleCheckpointScript;
    }
}
