﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberBoxes : MonoBehaviour
{
    private static NumberBoxes _instance;

    public static NumberBoxes Instance
    {
        get
        {
            return _instance;
        }
    }

    private int boxes = 0;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public void IncreaseBoxes()
    {
        boxes++;
        Debug.Log("Number of boxes:" + boxes);
    }

    public void ReduceBoxes()
    {
        boxes--;
        Debug.Log("Number of boxes:" + boxes);
    }
}
