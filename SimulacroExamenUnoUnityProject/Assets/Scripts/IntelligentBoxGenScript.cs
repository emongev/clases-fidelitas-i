﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntelligentBoxGenScript : MonoBehaviour
{

    [SerializeField]
    private GameObject _boxPrefab;

    private GameObject _generatedBox;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GenerateBox();
    }

    private void GenerateBox()
    {
        if (_generatedBox == null)
        {
            _generatedBox = Instantiate(_boxPrefab, this.transform.position, Quaternion.identity, null);
        }
    }
}
