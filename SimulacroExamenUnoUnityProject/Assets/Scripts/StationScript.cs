﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationScript : MonoBehaviour
{
    [SerializeField]
    private Transform grabBoxTestTransform;

    [SerializeField]
    private GameObject door;

    [SerializeField]
    private int numberBoxes;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(grabBoxTestTransform.position, new Vector2(3f, 3f), 0f, Vector2.zero);

        int cajas = 0;

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject.CompareTag("Caja"))
            {
                cajas++;
            }
        }

        if(cajas >= numberBoxes)
        {
            door.SetActive(false);
        }
    }
}
