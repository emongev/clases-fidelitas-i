﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersistenceExample : MonoBehaviour
{
    private const string POINTS_KEY = "PointsKey";

    [SerializeField]
    private Button _saveButton;

    [SerializeField]
    private Button _loadButton;

    [SerializeField]
    private Button _increaseButton;

    [SerializeField]
    private Button _resetButton;

    [SerializeField]
    private Text _pointsText;

    private int _currentLoadedPoints = 0;

    // Start is called before the first frame update
    void Start()
    {
        _saveButton.onClick.AddListener(OnSaveButtonPressed);
        _loadButton.onClick.AddListener(OnLoadButtonPressed);

        _increaseButton.onClick.AddListener(OnIncreaseButtonPressed);
        _resetButton.onClick.AddListener(OnResetButtonPressed);
    }

    private void OnSaveButtonPressed()
    {
        PlayerPrefs.SetInt(POINTS_KEY, _currentLoadedPoints);
    }

    private void OnLoadButtonPressed()
    {
        _currentLoadedPoints = PlayerPrefs.GetInt(POINTS_KEY, 0);

        _pointsText.text = _currentLoadedPoints.ToString();
    }

    private void OnIncreaseButtonPressed()
    {
        _currentLoadedPoints++;
        _pointsText.text = _currentLoadedPoints.ToString();
    }

    private void OnResetButtonPressed()
    {
        _currentLoadedPoints = 0;
        _pointsText.text = _currentLoadedPoints.ToString();
    }
}
