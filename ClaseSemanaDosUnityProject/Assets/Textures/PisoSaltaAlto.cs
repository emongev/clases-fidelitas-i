﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoSaltaAlto : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Me tocó algo! AGH! Autodestruyendome!");
        //Destroy(this.gameObject);

        Debug.Log("JUMP HIGH!");

        collision.gameObject.GetComponent<BichitoMovement>().JumpVeryHigh();

        Destroy(this.gameObject);
    }
}
