﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum AudioFile
    {
        Coin,
        Jump,
    }

    private static AudioManager _instance;

    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }
    }

    [SerializeField]
    private List<AudioClip> _audioClips;

    private List<AudioSource> _audioSources;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            DontDestroyOnLoad(this.gameObject);
            return;
        }

        GenerateAudioSources();
    }

    private void GenerateAudioSources()
    {
        _audioSources = new List<AudioSource>();

        AudioSource newAS = this.gameObject.AddComponent<AudioSource>();
        _audioSources.Add(newAS);

        //for (int i = 0; i < 5; i++)
        //{
        //    AudioSource newAS = this.gameObject.AddComponent<AudioSource>();
        //    _audioSources.Add(newAS); 
        //}
    }

    public void PlayAudio(AudioFile pAudioFile)
    {
        for (int i = 0; i < _audioSources.Count; i++)
        {
            if (!_audioSources[i].isPlaying)
            {
                _audioSources[i].clip = _audioClips[(int)pAudioFile];
                _audioSources[i].Play();
                break;
            }
        }
    }
}
