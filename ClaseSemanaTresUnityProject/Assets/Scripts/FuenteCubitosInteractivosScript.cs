﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuenteCubitosInteractivosScript : MonoBehaviour
{
    //Ocupamos referencia al prefab y asignarla en inspector
    public GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        //Se usa Instantiate con parametro el gameobject que quiero crear
        GameObject cubitoInteractivo = GameObject.Instantiate(prefab);

        //esto para ponerle la posicion a mano
        cubitoInteractivo.transform.position = new Vector3(0, 3f, 0);
        cubitoInteractivo.name = "Juancito Interactivo";
    }

    public float cadaCuantoTiroBichoNuevo = 2.5f;
    private float _tiempoHastaElMomento = 0f;

    // Update is called once per frame
    void Update()
    {
        _tiempoHastaElMomento += Time.deltaTime;

        if (_tiempoHastaElMomento > cadaCuantoTiroBichoNuevo)
        {
            _tiempoHastaElMomento -= cadaCuantoTiroBichoNuevo;

            //Se usa Instantiate con parametro el gameobject que quiero crear
            GameObject cubitoInteractivo = GameObject.Instantiate(prefab);

            //esto para ponerle la posicion a mano
            cubitoInteractivo.transform.position = new Vector3(0, 3f, 0);

            Rigidbody cubitoRigidbody = cubitoInteractivo.GetComponent<Rigidbody>();
            float aleatorioX = UnityEngine.Random.Range(-5, 5);
            float aleatorioZ = UnityEngine.Random.Range(-5, 5);

            cubitoRigidbody.velocity = new Vector3(aleatorioX, 2f, aleatorioZ);
        }
    }
}
